﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;

namespace Eksamensopgave2017.Utilities
{
    public abstract class TransactionBase
    {
        static uint _idCount;
        User _userToService;
        string _user;
        DateTime _date = DateTime.Now;
        decimal _balance = 0;

        public string User
        {
            get { return _user; }
            private set { _user = value ?? _user; }
        }
        public User UserToService { get { return _userToService; } set { _userToService = value; } }
        public uint ID { get; }
        public DateTime Date { get { return _date; } }

        public decimal Amount
        {
            get { return _balance; }
            set { _balance = value;  }
        }

        protected TransactionBase(User user, decimal transactionPrice)
        {
            ID = _idCount++;
            UserToService = user;
            User = UserToService.UserName;
            Amount = transactionPrice;
        }

        public abstract override string ToString();

        public abstract void Execute();

    }
}
