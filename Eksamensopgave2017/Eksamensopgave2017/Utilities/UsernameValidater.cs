﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksamensopgave2017.Utilities
{
    public class UsernameValidater
    {
        public static bool ValidateUserName(string username)
        {
            if (username.Any(c => (!char.IsLetterOrDigit(c) && c != '_' || char.IsUpper(c))))
            {
                return false;
            }
            return true;
        }
    }
}
