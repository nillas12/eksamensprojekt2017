﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksamensopgave2017.Utilities
{
    public class EmailValidater
    {
        public static bool ValidateEmail(string email)
        {
            if (!email.Contains('@'))
            {
                return false;
            }
            string[] emailParts = email.Split('@');
            if (emailParts[0].Length < 1 || emailParts[1].Length < 1)
            {
                return false;
            }
            if (ValidateLocalPart(emailParts[0]) && ValidateDomainPart(emailParts[1]))
            {
                return true;
            }
            return false;
        }
        private static bool ValidateLocalPart(string firstPart)
        {
            return !(firstPart.Any(c => (char.IsLetterOrDigit(c) && ValidFirstChars(c))));
        }

        private static bool ValidFirstChars(char c)
        {
            return (c == '_' || c == '.' || c == '-');
        }

        private static bool ValidLastChars(char c)
        {
            return (c == '.' || c == '-');
        }

        private static bool ValidateDomainPart(string lastPart)
        {
            if (ValidBeginEnd(lastPart) && lastPart.Contains('.') &&
                lastPart.Any(c => (char.IsLetterOrDigit(c))))
                return true;
            return false;
        }

        private static bool ValidBeginEnd(string toValidate)
        {
            if (toValidate.StartsWith("-") || toValidate.StartsWith(".") || 
                toValidate.EndsWith("-") || toValidate.EndsWith("."))
            {
                return false;
            }
            return true;
        }
    }
}
