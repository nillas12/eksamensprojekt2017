﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eksamensopgave2017.Utilities
{
    public class DirectoryGetter
    {
        public static string GetDirectory(string toAdd)
        {
            string p = Path.GetDirectoryName(Assembly.GetExecutingAssembly()
                    .EscapedCodeBase.Split(new[] {"file:"}, StringSplitOptions.None)[1])
                ?.Replace("\\\\", "");
            return Path.Combine(p, toAdd);
        }
    }
}
