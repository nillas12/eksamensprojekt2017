﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;

namespace Eksamensopgave2017.Utilities
{
    public class FileRead
    {
        private string _productPath = DirectoryGetter.GetDirectory("data\\products.csv");
        private string _userPath = DirectoryGetter.GetDirectory("data\\users.csv");

        public string ProductsPath { get { return _productPath; } }
        public string UsersPath { get { return _userPath; } }

        private List<string> ReadFile(string pathToRead, List<string> lines)
        {
            using (StreamReader r = new StreamReader(pathToRead))
            {
                r.ReadLine();
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            return lines;
        }

        public void ProductsToList(List<Product> lp)
        {
            List<string> lines = new List<string>();
            ReadFile(ProductsPath, lines);
            lines = HtmlTagRemove(lines);
            foreach (var line in lines)
            {
                lp.Add(ProductFromFile(line));
            }
        }

        public void UsersToList(List<User> lu)
        {
            List<string> lines = new List<string>();
            ReadFile(UsersPath, lines);
            foreach (var line in lines)
            {
                lu.Add(UserFromFile(line));
            }
        }

        private Product ProductFromFile(string line)
        {
            string[] l = line.Split(';');
            string productname = l[1];
            decimal price = decimal.Parse(l[2]);
            bool active = l[3] == "1";
            Product p = new Product(l[1], price, active);
            return p;
        }

        private User UserFromFile(string line)
        {
            line = StripCitation(line);
            string[] l = line.Split(';');
            string firstname = l[1];
            string lastname = l[2];
            string username = l[3];
            string email = l[4];
            decimal balance = decimal.Parse(l[5]);
            User u = new User(firstname, lastname, username, email, balance);
            return u;
        }

        private List<string> HtmlTagRemove(List<string> lines)
        {
            List<string> resultList = new List<string>();
            foreach (var line in lines)
            { 
                string temp = StripHtml(line);
                temp = StripCitation(temp);
                resultList.Add(temp);
            }
            return resultList;
        }
        private static string StripHtml(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        private static string StripCitation(string input)
        {
            return Regex.Replace(input, "\"", "");
        }
    }
}
