﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;

namespace Eksamensopgave2017.Utilities
{
    public class InsufficientFundsException : Exception
    {
        public BuyTransaction TransactionBuyException { get; }
        public InsufficientFundsException(BuyTransaction transaction) : base
            ($"Transaction {transaction.ID} failed. Insufficient funds")
        {
            TransactionBuyException = transaction;
        }
    }

    public class InactiveProductException : Exception
    {
        public BuyTransaction InactiveProductTransaction { get; }
        public InactiveProductException(BuyTransaction transaction) : base(
            $"Transaction {transaction.ID} failed. Product is inactive")
        {
            InactiveProductTransaction = transaction;
        }
    }
}
