﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017.Core
{
    public class BuyTransaction : TransactionBase
    {
        private Product _product;

        public Product ProductToPurchase { get; }
        public BuyTransaction(User user, Product product) : base(user, product.Price)
        {
            Amount = product.Price;
            ProductToPurchase = product;
        }

        public override void Execute()
        {
            if (!ProductToPurchase.IsActive())
            {
                throw new InactiveProductException(this);
            }
            if (UserToService.Balance < Amount)
            {
                throw new InsufficientFundsException(this);
            }
            UserToService.Balance = UserToService.Balance - this.Amount;
        }
        public override string ToString()
        {
            return $"Subtract from {ID} {User} {Amount} {Date}";
        }
    }
}
