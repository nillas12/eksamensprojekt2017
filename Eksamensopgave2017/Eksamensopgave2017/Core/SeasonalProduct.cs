﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksamensopgave2017.Core
{
    public class SeasonalProduct : Product
    {
        private DateTime _seasonStartDate = DateTime.Today;
        private DateTime _seasonEndDate = DateTime.Today;

        public DateTime SeasonStartDate { get; set; }
        public new bool Active { get; private set; }
        public DateTime SeasonEndDate { get; set; }
        public SeasonalProduct(string name, decimal price, bool active, bool canBeBoughtOnCredit, DateTime seasonStartDate, DateTime seasonEndDate) : base(name, price, active, canBeBoughtOnCredit)
        {
            Name = name;
            Price = price;
            SeasonStartDate = seasonStartDate;
            SeasonEndDate = seasonEndDate;
            CanBeBoughtOnCredit = canBeBoughtOnCredit;
            DateActiveCheck();
        }
        private void DateActiveCheck()
        {
            if ((DateTime.Now >= SeasonStartDate && DateTime.Now <= SeasonEndDate))
            {
                Active = true;
            }
            Active = false;
        }
        
    }
}
