﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eksamensopgave2017.Core
{
    public class Product
    {
        protected static uint _idCount = 1;
        private string _name = "";
        private decimal _price = 0;
        private bool _active = false;
        private bool _canBeBoughtOnCredit = false;

        public string Name
        {
            get { return _name; }
            protected set { _name = value ?? _name; }
        }
        public uint ID { get; }

        public decimal Price { get; protected set; }

        protected bool Active { get; set; }

        public bool CanBeBoughtOnCredit { get; set; }

        public Product(string name, decimal price, bool active, bool canBeBoughtOnCredit)
        {
            ID = _idCount++;
            Name = name;
            Price = price;
            Active = active;
            CanBeBoughtOnCredit = canBeBoughtOnCredit;
        }
        public Product(string name, decimal price, bool active) : this(name, price, active, false)
        {
        }
        public Product(string name, decimal price) : this(name, price, false, false)
        {
        }
        public bool IsActive()
        {
            return Active;
        }
        public override string ToString()
        {
            return $"{ID} {Name} {Price}";
        }
    }
}
