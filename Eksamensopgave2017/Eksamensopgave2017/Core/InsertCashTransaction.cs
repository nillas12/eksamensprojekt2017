﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017.Core
{
    public class InsertCashTransaction : TransactionBase
    {
        public InsertCashTransaction(User user, decimal cashToAdd) : base(user, cashToAdd)
        {
        }

        public override void Execute()
        {
            UserToService.Balance += Amount;
        }

        public override string ToString()
        {
            return $"Insert to {UserToService.ID} {User} {Amount} {Date}";
        }
    }
}
