﻿using System;
using System.Collections.Generic;
using Eksamensopgave2017.Core;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017
{
    public interface IStregsystem
    {
        IEnumerable<Product> ActiveProducts { get; }
        InsertCashTransaction AddCreditsToAccount(User user, int amount);
        BuyTransaction BuyProduct(User user, Product product);
        Product GetProductByid(int productid);
        IEnumerable<TransactionBase> GetTransactions(User user, int count);
        User GetUser(Func<User, bool> predicate);
        User GetUserByUsername(string username);
        event User.UserBalanceNotification UserBalanceWarning;
    }
}