﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Eksamensopgave2017.Core;
using Eksamensopgave2017.Utilities;
using NUnit.Framework;

namespace Eksamensopgave2017.Core
{
    public class Stregsystem : IStregsystem
    {
        private List<Product> _plist;
        private List<User> _ulist;
        private List<TransactionBase> _tList;

        public IEnumerable<Product> ActiveProducts => PList.Where(p => p.IsActive());

        public List<Product> PList { get { return _plist; } private set { _plist = value; } }
        public List<User> UList { get { return _ulist; } private set { _ulist = value; } }
        public List<TransactionBase> TList { get { return _tList; } private set { _tList = value; } }

        public Stregsystem()
        {
            FileRead fr = new FileRead();
            List<Product> plist = new List<Product>();
            List<User> ulist = new List<User>();
            List<TransactionBase> tlist = new List<TransactionBase>();
            fr.ProductsToList(plist);
            fr.UsersToList(ulist);
            PList = plist;
            UList = ulist;
            TList = tlist;
        }

        public event User.UserBalanceNotification UserBalanceWarning;


        public InsertCashTransaction AddCreditsToAccount(User user, int amount)
        {
            InsertCashTransaction addCashTransaction = new InsertCashTransaction(user, amount);
            return addCashTransaction;
        }

        public BuyTransaction BuyProduct(User user, Product product)
        {
            BuyTransaction buyTransaction = new BuyTransaction(user, product);
            return buyTransaction;
        }

        public Product GetProductByid(int productid)
        {
            //return PList[productid - 1];
            foreach (var p in PList)
            {
                if (p.ID == productid)
                {
                    return p;
                }
            }
            return null;
        }

        public IEnumerable<TransactionBase> GetTransactions(User user, int count)
        {
            return TList.OrderBy(x => x.Date).Where(t => t.UserToService == user).Take(count);
        }

        public User GetUser(Func<User, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public User GetUserByUsername(string username)
        {
            /*Der findes nok en pænere måde at gøre det her på :D*/
            return UList.First(x => x.UserName == username);
        }
    }
}