﻿using System;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017.Core
{
    public class User : IComparable<User>, IEquatable<User>
    {
        private static uint _idCount = 1;
        private string _firstName = "";
        private string _lastName = "";
        private string _userName = "";
        private string _email = "";
        private decimal _balance = 0;

        public string FirstName
        {
            get { return _firstName; }
            private set { _firstName = value ?? _firstName; }
        }
        public uint ID { get; }

        public string LastName
        {
            get { return _lastName; }
            private set { _lastName = value ?? _lastName; }
        }
        public decimal Balance { get; internal set; }

        public string UserName
        {
            get { return this._userName; }
            private set { _userName = UsernameValidater.ValidateUserName(value) ? value : _userName; }
        }

        public string Email
        {
            get { return _email; }
            private set { _email = EmailValidater.ValidateEmail(value) ? value : _email; }
        }

        public User(string firstname, string lastname, string username,
            string email, decimal balance)
        {
            ID = _idCount++;
            FirstName = firstname;
            LastName = lastname;
            UserName = username;
            Email = email;
            Balance = balance;
        }

        public delegate void UserBalanceNotification(User user, decimal balance);
        public int CompareTo(User other) => ID.CompareTo(other.ID);
        public override string ToString() => $"{ID} {FirstName} {LastName} {Email}";

        public bool Equals(User other)
        {
            if (other == null) return false;
            return string.Equals(FirstName, other.FirstName) &&
                   string.Equals(LastName, other.LastName) &&
                   string.Equals(UserName, other.UserName) &&
                   string.Equals(Email, other.Email) &&
                   ID == other.ID &&
                   Balance == other.Balance;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Balance.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) ID;
                hashCode = (hashCode * 397) ^ Balance.GetHashCode();
                return hashCode;
            }
        }

        

    }
}
