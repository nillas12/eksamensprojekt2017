﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;
using NUnit.Framework;


namespace Eksamensopgave2017.Test.Core
{
    [TestFixture]
    class UserTest
    {
       
        [Test]
        public void CreateValidUser()
        {
            User validUser = new User( "Nichlas", "Lisby", "nlisby16","nillas12@gmail.com", 1000);
            Assert.AreEqual(validUser.FirstName, "Nichlas");
            Assert.AreEqual(validUser.LastName, "Lisby");
            Assert.AreEqual(validUser.UserName, "nlisby16");
            Assert.AreEqual(validUser.Email, "nillas12@gmail.com");
            Assert.AreEqual(validUser.Balance, 1000);
        }

        [Test]
        public void UsersToString()
        {
            User validUser = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 1000);
            Assert.AreEqual($"{validUser.ID} Nichlas Lisby nillas12@gmail.com", validUser.ToString());
        }

        [Test]
        public void CreateInvalidUser()
        {
            User invalidUser = new User( "Nichlas", "Lisby", "nlisby16", "nillas12@gmailcom", 1000);
            Assert.AreEqual(invalidUser.FirstName, "Nichlas");
            Assert.AreNotEqual("nillas12@gmailcom", invalidUser.Email);
        }

        [Test]
        public void CorrectIDAssignment()
        {
            User u1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 1000);
            User u2 = new User("Knud", "Jensen", "kj12", "kj@hotmail.com", 5000);
            User u3 = new User("Tom", "Thomsen", "tm16", "tm@nicemail.net", 21000);
            Assert.AreEqual(u1.ID+1, u2.ID);
            Assert.AreEqual(u2.ID+1, u3.ID);
            Assert.AreEqual(u2.ID-1, u1.ID);
        }

        [Test]
        public void SortUsersByID()
        {
            User u1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 1000);
            User u2 = new User("Knud", "Jensen", "kj12", "kj@hotmail.com", 5000);
            User u3 = new User("Tom", "Thomsen", "tm16", "tm@nicemail.net", 21000);
            List<User> uList = new List<User>() {u3, u2, u1};
            Assert.AreEqual(uList[0].ID, u3.ID);
            uList.Sort();
            Assert.AreEqual(uList[0].FirstName, u1.FirstName);
        }
    }
}
