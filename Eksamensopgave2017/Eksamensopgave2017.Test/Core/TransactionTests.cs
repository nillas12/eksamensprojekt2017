﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;
using Eksamensopgave2017.Utilities;
using NUnit.Framework;

namespace Eksamensopgave2017.Test.Core
{
    [TestFixture]
    class TransactionTests
    {
        [Test]
        public void InsertCashTransactionTest()
        {
            User user1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 0);
            InsertCashTransaction insertTransaction1 = new InsertCashTransaction(user1, 1000);
            insertTransaction1.Execute();
            Assert.AreEqual(1000, user1.Balance);
        }

        [Test]
        public void MakePurchaseTest()
        {
            User user1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 0);
            InsertCashTransaction insertTransaction1 = new InsertCashTransaction(user1, 1000);
            insertTransaction1.Execute();
            Product product = new Product("Coca-Cola", 500, true, false);
            BuyTransaction buyTransaction = new BuyTransaction(user1, product);
            buyTransaction.Execute();
            Assert.AreEqual(500, user1.Balance);
        }
        [Test]
        public void TransactionToStringTest()
        {
            User user10 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 0);
            InsertCashTransaction insertTransaction1 = new InsertCashTransaction(user10, 1000);
            insertTransaction1.Execute();
            Assert.AreEqual($"Insert to {user10.ID} nlisby16 1000 {DateTime.Now}", insertTransaction1.ToString());
        }

        [Test]
        public void InactiveProductExceptionTest()
        {
            User user1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 10000);
            Product product = new Product("Coca-Cola", 500, false, false);
            BuyTransaction buyTransaction = new BuyTransaction(user1, product);
            Assert.Throws(typeof(InactiveProductException), buyTransaction.Execute);
        }

        [Test]
        public void InsufficientFundsExceptionTest()
        {
            User user1 = new User("Nichlas", "Lisby", "nlisby16", "nillas12@gmail.com", 10000);
            Product product = new Product("Coca-Cola", 50000, true, false);
            BuyTransaction buyTransaction = new BuyTransaction(user1, product);
            Assert.Throws(typeof(InsufficientFundsException), buyTransaction.Execute);
        }
    }
}
