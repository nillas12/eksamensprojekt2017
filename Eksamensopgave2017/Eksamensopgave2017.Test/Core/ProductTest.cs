﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Eksamensopgave2017.Core;

namespace Eksamensopgave2017.Test.Core
{
    [TestFixture]
    class ProductTest
    {
        [Test]
        public void CreateValidProduct()
        {
            Product validProduct = new Product("Coca-Cola", 2000, true, true);
            Assert.AreEqual("Coca-Cola", validProduct.Name);
            Assert.AreEqual(2000, validProduct.Price);
            Assert.AreEqual(true, validProduct.IsActive());
            Assert.AreEqual(true, validProduct.CanBeBoughtOnCredit);
        }

        [Test]
        public void CreateValidProductWithActiveFlag()
        {
            Product validProduct = new Product("Sprite", 2000, true);
            Assert.AreEqual("Sprite", validProduct.Name);
            Assert.AreEqual(2000, validProduct.Price);
            Assert.AreEqual(true, validProduct.IsActive());
            Assert.AreEqual(false, validProduct.CanBeBoughtOnCredit);
        }

        [Test]
        public void CreateValidProductWithoutFlags()
        {
            Product validProduct = new Product("Sprite", 2000);
            Assert.AreEqual("Sprite", validProduct.Name);
            Assert.AreEqual(2000, validProduct.Price);
            Assert.AreEqual(false, validProduct.IsActive());
            Assert.AreEqual(false, validProduct.CanBeBoughtOnCredit);
        }

        [Test]
        public void CreateSeasonalProductTest()
        {
            Product seasonalProduct = new SeasonalProduct("Coca-Cola Lime", 1000, true, true, DateTime.Today.AddDays(10), DateTime.Today.AddYears(1));
            Assert.AreEqual("Coca-Cola Lime", seasonalProduct.Name);
            Assert.AreEqual(1000, seasonalProduct.Price);
            Assert.AreEqual(true, seasonalProduct.CanBeBoughtOnCredit);
            Assert.AreEqual(true, seasonalProduct.IsActive());
        }
        [Test]
        public void CreateProductList()
        {
            Product seasonalProduct = new SeasonalProduct("Coca-Cola Lime", 1000, true, true, DateTime.MinValue, DateTime.Today.AddYears(1));
            Product validProduct = new Product("Coca-Cola", 2000, true, true);
            List<Product> plistProducts = new List<Product>();
            plistProducts.Add(seasonalProduct);
            plistProducts.Add(validProduct);
            Assert.AreEqual("Coca-Cola Lime", plistProducts[0].Name);
            Assert.AreEqual(1000, plistProducts[0].Price);
            Assert.AreEqual(true, plistProducts[0].CanBeBoughtOnCredit);
            Assert.AreEqual(true, plistProducts[0].IsActive());
            Assert.AreEqual("Coca-Cola", validProduct.Name);
            Assert.AreEqual(2000, validProduct.Price);
            Assert.AreEqual(true, validProduct.IsActive());
            Assert.AreEqual(true, validProduct.CanBeBoughtOnCredit);

        }
    }
}
