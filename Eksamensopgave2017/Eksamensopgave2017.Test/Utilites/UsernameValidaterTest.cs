﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017.Test.Utilites
{
    [TestFixture]
    class UsernameValidaterTest
    {
        [TestCase("nillas12", true)]
        [TestCase("nillas12_", true)]
        [TestCase("nillas.12", false)]
        [TestCase("nillas-12", false)]
        [TestCase("Nillas12", false)]
        [TestCase("Nillas12", false)]
        public void UsernameValidateTest(string email, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, UsernameValidater.ValidateUserName(email));
        }
    }
}
