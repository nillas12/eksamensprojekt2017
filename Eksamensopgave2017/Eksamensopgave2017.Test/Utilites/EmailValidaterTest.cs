﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using Eksamensopgave2017.Utilities;

namespace Eksamensopgave2017.Test.Utilites
{
    [TestFixture]
    public class EmailValidaterTest
    {
        [TestCase("nillas12@gmail.com", true)]
        [TestCase("nIllas12@gmail.com", true)]
        [TestCase("nillas12@.gmail.com", false)]
        [TestCase("nillas12@-gmail.com", false)]
        [TestCase("nillas12@_gmail.com", true)]
        [TestCase("nillas12@gmailcom", false)]
        [TestCase("¤@gmailcom", false)]
        [TestCase("@gmailcom", false)]
        [TestCase("nillas12@.", false)]
        public void EmailValidateTest(string email, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, EmailValidater.ValidateEmail(email));
        }
    }
}
