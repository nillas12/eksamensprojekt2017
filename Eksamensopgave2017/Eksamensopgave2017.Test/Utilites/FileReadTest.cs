﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eksamensopgave2017.Core;
using Eksamensopgave2017.Utilities;
using NUnit.Framework;
using System.IO;
using System.Reflection;

namespace Eksamensopgave2017.Test.Utilites
{
    [TestFixture]
    class FileReadTest
    {
        FileRead fr4 = new FileRead();
        [Test]
        public void UserToListTest()
        {
            List<User> ulist = new List<User>();
            fr4.UsersToList(ulist);
            Assert.AreEqual("Filippa", ulist[0].FirstName);
            Assert.AreEqual("Børge", ulist[1].FirstName);
            Assert.AreEqual(624, ulist[0].Balance);
        }

        [Test]
        public void ProductToListTest()
        {
            List<Product> plist = new List<Product>();
            fr4.ProductsToList(plist);
            Assert.AreEqual(false, plist[0].IsActive());
            Assert.AreEqual("Diverse", plist[0].Name);
            Assert.AreEqual(100 ,plist[0].Price);
        }
    }
}
